﻿using System;
using System.Collections.Generic;
using Verse;
using RimWorld;

namespace Rimworld_Animations_Patch
{
    public class CompProperties_ApparelVisibility : CompProperties
    {
        public CompProperties_ApparelVisibility()
        {
            base.compClass = typeof(CompApparelVisibility);
        }
    }
}
