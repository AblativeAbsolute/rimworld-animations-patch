﻿using System.Collections.Generic;
using System.Linq;
using Verse;
using Verse.AI;
using Verse.AI.Group;
using RimWorld;
using rjw;
using RJWSexperience.Ideology;
using UnityEngine;

namespace Rimworld_Animations_Patch
{
    public static class SexInteractionUtility
    {
		public static bool PawnCaughtLovinByWitness(Pawn pawn, Pawn witness)
		{
			if (witness == null || pawn == witness || witness.AnimalOrWildMan() || witness.RaceProps.IsMechanoid || witness.Awake() == false || witness.CanSee(pawn) == false)
			{ return false; }

			if (pawn.IsHavingSex() == false && pawn.IsMasturbating() == false)
			{ return false; }

			List<Pawn> sexParticipants = pawn.GetAllSexParticipants();
			bool witnessIsCourtingSexParticipant = witness.jobs.curDriver is JobDriver_SexBaseInitiator && sexParticipants.Contains((witness.jobs.curDriver as JobDriver_SexBaseInitiator).Partner);

			if (sexParticipants.Contains(witness) || witnessIsCourtingSexParticipant)
			{ return false; }

			return true;
		}

		public static bool PawnIsCheatingOnPartner(Pawn pawn, Pawn partner)
		{
			if (BasicSettings.worryAboutInfidelity == false || pawn.IsMasturbating() || pawn.IsHavingSex() == false || pawn.GetAllSexParticipants().Contains(partner))
			{ return false; }

			if (pawn.GetAllSexParticipants().Any(x => pawn.GetSpouseCount(false) > 0 && pawn.GetSpouses(false).Contains(x)))
			{ return false; }

			return partner.IsLoverOfOther(pawn) && pawn.HasTrait("Polygamist") == false && partner.HasTrait("Polygamist") == false;
		}

		public static bool PawnCanInvitePasserbyForSex(Pawn passerby, List<Pawn> participants)
		{
			if (passerby == null || participants.NullOrEmpty() || participants.Contains(passerby) || passerby.AnimalOrWildMan() || passerby.RaceProps.IsMechanoid || passerby.Awake() == false || participants.All(x => x.CanSee(passerby) == false))
			{ return false; }

			if (participants.Any(x => x.IsForbidden(passerby) || x.HostileTo(passerby) || PawnIsCheatingOnPartner(x, passerby)) || CasualSex_Helper.CanHaveSex(passerby) == false || xxx.IsTargetPawnOkay(passerby) == false || participants.Count > 2)
			{ return false; }

			if (SexUtility.ReadyForHookup(passerby) &&
				(passerby?.jobs?.curJob == null || (passerby.jobs.curJob.playerForced == false && CasualSex_Helper.quickieAllowedJobs.Contains(passerby.jobs.curJob.def))) &&
				participants.Any(x => SexAppraiser.would_fuck(x, passerby) > 0.1f && SexAppraiser.would_fuck(passerby, x) > 0.1f) &&
				participants.All(x => SexAppraiser.would_fuck(x, passerby, false, false, true) > 0.1f && SexAppraiser.would_fuck(passerby, x, false, false, true) > 0.1f))
			{
				return true;
			}

			return false;
		}

		public static TabooStatus CheckSexJobAgainstMorals(Pawn pawn, JobDriver_Sex jobDriver, out Precept precept)
		{		
			bool sexIsNecro = jobDriver.Partner != null && jobDriver.Partner.Dead;
			bool sexIsBeastial = jobDriver.Partner != null && jobDriver.Partner.RaceProps.Animal;
			bool sexIsRape = sexIsBeastial == false && sexIsNecro == false && 
				(jobDriver is JobDriver_Rape || jobDriver is JobDriver_RapeEnemy || jobDriver is JobDriver_SexBaseRecieverRaped) && 
				jobDriver.Partner.IsPrisoner == false && jobDriver.Partner.IsSlave == false;
			bool sexIsSlaveRape = sexIsBeastial == false && sexIsNecro == false &&
				(jobDriver is JobDriver_Rape || jobDriver is JobDriver_RapeEnemy || jobDriver is JobDriver_SexBaseRecieverRaped) &&
				(jobDriver.Partner.IsPrisoner || jobDriver.Partner.IsSlave);
			bool sexIsXeno = jobDriver.Partner != null && jobDriver.Partner.def.defName != jobDriver.pawn.def.defName;

			TabooStatus tabooStatus = TabooStatus.NotTaboo;
			precept = null;

			if (BasicSettings.worryAboutNecro && sexIsNecro && xxx.is_necrophiliac(pawn) == false)
			{ tabooStatus = GetTabooStatusOfIssue(pawn, DefDatabase<IssueDef>.GetNamedSilentFail("Necrophilia"), TabooStatus.MajorTaboo, out precept); }

			else if (BasicSettings.worryAboutBeastiality && sexIsBeastial && xxx.is_zoophile(pawn) == false)
			{ tabooStatus = GetTabooStatusOfIssue(pawn, DefDatabase<IssueDef>.GetNamedSilentFail("Beastility"), TabooStatus.MajorTaboo, out precept); }

			else if (BasicSettings.worryAboutRape && sexIsRape && xxx.is_rapist(pawn) == false)
			{ tabooStatus = GetTabooStatusOfIssue(pawn, DefDatabase<IssueDef>.GetNamedSilentFail("Rape"), TabooStatus.MajorTaboo, out precept); }

			else if (BasicSettings.worryAboutRape && BasicSettings.ignoreSlaveRape == false && sexIsSlaveRape && xxx.is_rapist(pawn) == false)
			{ tabooStatus = GetTabooStatusOfIssue(pawn, DefDatabase<IssueDef>.GetNamedSilentFail("Rape"), TabooStatus.MajorTaboo, out precept); }

			else if (BasicSettings.worryAboutXeno && sexIsXeno && pawn.HasTrait("Xenophobia") && pawn.story.traits.DegreeOfTrait(DefDatabase<TraitDef>.GetNamedSilentFail("Xenophobia")) > 0)
			{ tabooStatus = TabooStatus.MajorTaboo; }

			else if (BasicSettings.worryAboutXeno && sexIsXeno)
			{ tabooStatus = GetTabooStatusOfIssue(pawn, DefDatabase<IssueDef>.GetNamedSilentFail("HAR_AlienDating"), TabooStatus.NotTaboo, out precept); }
	
			//DebugMode.Message("Sex job is: " + jobDriver + " Issue is: " + (precept?.def?.issue?.defName).ToStringSafe() + " Opinion is: " + (precept?.def?.defName).ToStringSafe() + " Judgement is: " + tabooStatus.ToString());

			return tabooStatus;
		}

		public static TabooStatus GetTabooStatusOfIssue(Pawn pawn, IssueDef issueDef, TabooStatus defaultTabboStatus, out Precept precept)
		{
			if (pawn.IssueIsMajorTaboo(issueDef, out precept))
			{ return TabooStatus.MajorTaboo; }

			if (pawn.IssueIsMinorTaboo(issueDef, out precept))
			{ return TabooStatus.MinorTaboo; }

			return defaultTabboStatus;
		}

		public static bool ResolveThoughtsForWhenSexIsWitnessed(Pawn pawn, Pawn witness, out bool witnessJoiningSex)
		{
			witnessJoiningSex = false;

			if (pawn.IsAnimal() || pawn.RaceProps.IsMechanoid || pawn.Dead)
			{ return false; }

			if (witness.IsAnimal() || witness.RaceProps.IsMechanoid || witness.Dead)
			{ return false; }

			JobDriver_Sex jobDriver = pawn.jobs.curDriver as JobDriver_Sex;

			string pawnThoughtDefName = pawn.IsMasturbating() ? "SeenMasturbating" : "SeenHavingSex";
			string witnessThoughtDefName = pawn.IsMasturbating() ? "SawMasturbation" : "SawSex";

			bool pawnIsExhibitionist = pawn.HasTrait("Exhibitionist") || xxx.has_quirk(pawn, "Exhibitionist");
			if (pawnIsExhibitionist)
			{ pawnThoughtDefName += "Exhibitionist"; }

			bool witnessIsVoyeur = witness.HasTrait("Voyeur") || xxx.has_quirk(witness, "Voyeur");
			if (witnessIsVoyeur)
			{ witnessThoughtDefName += "Voyeur"; }

			bool sexIsRitual = pawn.GetLord() != null && pawn.GetLord().LordJob is LordJob_Ritual && witness?.Ideo == pawn?.Ideo;
			bool pawnIsVictim = pawn.CurJob.def == xxx.gettin_raped || pawn.Dead;
			bool pawnIsCheating = pawnIsVictim == false && PawnIsCheatingOnPartner(pawn, witness);

			witnessJoiningSex = Random.value < BasicSettings.chanceForOtherToJoinInSex && PawnCanInvitePasserbyForSex(witness, pawn.GetAllSexParticipants());

			// Determine if there are any issues with the witness' morals
			TabooStatus tabooStatus = CheckSexJobAgainstMorals(witness, jobDriver, out Precept precept);

			if (tabooStatus == TabooStatus.MajorTaboo)
			{ witnessThoughtDefName = "SawMajorTaboo"; witnessJoiningSex = false; }
			
			else if (tabooStatus == TabooStatus.MinorTaboo)
			{ witnessThoughtDefName = "SawTaboo"; witnessJoiningSex = false; }

			else if (pawnIsCheating)
			{ witnessThoughtDefName = "CheatedOnMe"; witnessJoiningSex = false; }

			else if (BasicSettings.needPrivacy == false)
			{ witnessThoughtDefName = ""; }

			// Apply thoughts to witness
			ThoughtDef witnessThoughtDef = DefDatabase<ThoughtDef>.GetNamedSilentFail(witnessThoughtDefName);

			if (witnessThoughtDef != null && pawnIsVictim == false && witnessJoiningSex == false && sexIsRitual == false)
			{
				witness.needs.mood.thoughts.memories.TryGainMemory(witnessThoughtDef, pawn, precept);

				if (witnessThoughtDef.stages[0].baseMoodEffect < 0)
				{ FleckMaker.ThrowMetaIcon(witness.Position, witness.Map, FleckDefOf.IncapIcon); }

				// Fight or flight reaction
				if (BasicSettings.majorTabooCanStartFights &&
					(tabooStatus == TabooStatus.MajorTaboo || pawnIsCheating) &&
					witness.Drafted == false && 
					witness.jobs.curDriver is JobDriver_Flee == false &&
					witness.jobs.curDriver is JobDriver_AttackMelee == false &&
					witness.jobs.curDriver is JobDriver_Vomit == false)
				{
					// Fight
					if (pawn.RaceProps.Humanlike && witness.RaceProps.Humanlike && witness.DislikesViolence() == false && (Random.value < 0.2f || witness.EnjoysViolence()) && witness.HostileTo(pawn) == false && InteractionUtility.TryGetRandomVerbForSocialFight(witness, out Verb verbToUse))
					{
						if (witness.LastAttackedTarget.Pawn != pawn || (pawn.mindState.lastAttackTargetTick < 0 && pawn.mindState.lastAttackTargetTick + Find.TickManager.TicksGame > 180))
						{
							pawn.mindState.lastAttackTargetTick = Find.TickManager.TicksGame;
							string message = witness.LabelShort + " is going to punish " + pawn.LabelShort + " for " + GenderUtility.GetPossessive(pawn.gender) + " transgression.";
							Messages.Message(message, pawn, MessageTypeDefOf.NegativeEvent);
						}

						Job job = JobMaker.MakeJob(JobDefOf.SocialFight, pawn);
						job.maxNumMeleeAttacks = 1;
						job.verbToUse = verbToUse;

						witness.jobs.EndCurrentJob(JobCondition.InterruptForced, false, false);
						witness.jobs.StartJob(job);
					}

					// Vomit
					else if (jobDriver.Partner != null && jobDriver.Partner.Dead)
					{
						Job jobVomit = JobMaker.MakeJob(JobDefOf.Vomit);
						Job jobFlee = JobMaker.MakeJob(JobDefOf.FleeAndCower, CellFinderLoose.GetFleeDest(witness, new List<Thing>() { pawn }, 24f), pawn);

						witness.jobs.EndCurrentJob(JobCondition.InterruptForced, false, false);
						witness.jobs.StartJob(jobVomit);
						witness.jobs.jobQueue.EnqueueFirst(jobFlee);
					}

					// Flight
					else
					{
						Job job = JobMaker.MakeJob(JobDefOf.FleeAndCower, CellFinderLoose.GetFleeDest(witness, new List<Thing>() { pawn }, 24f), pawn);
						witness.jobs.EndCurrentJob(JobCondition.InterruptForced, false, false);
						witness.jobs.StartJob(job);
					}
				}
			}

			// Check issue against pawn precepts
			tabooStatus = CheckSexJobAgainstMorals(pawn, jobDriver, out precept);

			if (tabooStatus == TabooStatus.MajorTaboo)
			{ pawnThoughtDefName = "SeenCommittingMajorTaboo"; witnessJoiningSex = false; }

			else if (tabooStatus == TabooStatus.MinorTaboo)
			{ pawnThoughtDefName = "SeenCommittingTaboo"; witnessJoiningSex = false; }

			else if (pawnIsCheating)
			{ pawnThoughtDefName = "CaughtCheating"; witnessJoiningSex = false; }

			else if (BasicSettings.needPrivacy == false)
			{ pawnThoughtDefName = ""; }

			// Apply thoughts to pawn
			ThoughtDef pawnThoughtDef = DefDatabase<ThoughtDef>.GetNamedSilentFail(pawnThoughtDefName);

			if (pawnThoughtDef != null && pawnIsVictim == false && witnessJoiningSex == false && sexIsRitual == false)
			{
				pawn.needs.mood.thoughts.memories.TryGainMemory(pawnThoughtDef, witness, precept);

				if (pawnThoughtDef.stages[0].baseMoodEffect < 0)
				{ FleckMaker.ThrowMetaIcon(pawn.Position, pawn.Map, FleckDefOf.IncapIcon); }
			}

			return witnessJoiningSex;
		}
	}
}
