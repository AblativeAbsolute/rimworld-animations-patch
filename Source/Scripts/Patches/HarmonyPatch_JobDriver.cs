﻿using System;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;
using Rimworld_Animations;
using rjw;

namespace Rimworld_Animations_Patch
{
	[HarmonyPatch(typeof(JobDriver), "GetReport")]
	public static class HarmonyPatch_JobDriver
	{
		public static bool Prefix(JobDriver __instance, ref string __result)
		{
			JobDriver_Sex jobdriver = __instance as JobDriver_Sex;

			if (jobdriver != null && jobdriver.pawn != null && jobdriver.pawn.GetAnimationData() != null && jobdriver.Sexprops.isRape == false && jobdriver.Sexprops.isWhoring == false)
			{
				LocalTargetInfo a = jobdriver.job.targetA.IsValid ? jobdriver.job.targetA : jobdriver.job.targetQueueA.FirstValid();
				LocalTargetInfo b = jobdriver.job.targetB.IsValid ? jobdriver.job.targetB : jobdriver.job.targetQueueB.FirstValid();
				LocalTargetInfo targetC = jobdriver.job.targetC;

				__result = JobUtility.GetResolvedJobReport(jobdriver.pawn.GetAnimationData().animationDef.label, a, b, targetC);

				return false;
			}

			return true;
		}
	}
}